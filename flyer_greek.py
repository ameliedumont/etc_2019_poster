from random import choice, random, randint
from flat import gray, rgb, cmyk, spot, diffuse, font, text, mesh, tree, \
    moveto, quadto, curveto, shape, strike, image, scene, group, document, view

d = document(210, 148.5, 'mm')
p = d.addpage()
p2 = d.addpage()
black = rgb(0, 0, 0)
purple = rgb(204, 0, 255)
blue = rgb(0, 153, 204)
green = rgb(0, 255, 153)


ciel_fond = image.open("ciel.jpg")
block_cielfond = p.place(ciel_fond)
block_cielfond.frame(-5,-5,200,70)
block_cielfond.fitwidth(198)

ciel = image.open("base_detouree.jpg")
block_ciel = p.place(ciel)
block_ciel.frame(89,40,200,70)
block_ciel.fitwidth(102)

for i in range(6,23):
    s = shape().stroke(purple).width(0.05)
    p2.place(s.circle(-i+203, i+7, i))

halt = image.open("bluu01.jpg")
block_halt = p2.place(halt)
block_halt.frame(160,10,200,70)
block_halt.fitwidth(30)

aand = image.open("bluu02.jpg")
block_and = p2.place(aand)
block_and.frame(164,22.5,200,70)
block_and.fitwidth(25)

catch = image.open("bluu03.jpg")
block_catch = p2.place(catch)
block_catch.frame(168,35,200,70)
block_catch.fitwidth(32)

fire = image.open("bluu04.jpg")
block_fire = p2.place(fire)
block_fire.frame(172,46.5,200,70)
block_fire.fitwidth(23)

t = shape().stroke(blue).width(1)
p.place(t.line(0,120,175,0))
p.place(t.line(25,148.5,190,0))
p.place(t.line(85,148.5,205,0))
p.place(t.line(145,148.5,210,17))
p.place(t.line(200,148.5,210,80))
p.place(t.line(148.5,160,297,230))
p.place(t.line(148.5,160,297,300))
p.place(t.line(148.5,160,297,420))
p.place(t.line(148.5,160,217,420))
p.place(t.line(0,120,210,120))
p.place(t.line(51,85,210,85))
p.place(t.line(95,55,210,55))
p.place(t.line(128,32,210,32))
p.place(t.line(153,15,210,15))
p.place(t.line(171,3,210,3))

regular = font.open('SportingGrotesque-Regular.otf')
titre_bis = strike(regular).size(48, 50).color(green)
title_bis = p.place(titre_bis.text("  /ETC \nEclectic \nTech \nCarnival"))
title_bis.frame(9,10,400,200)

titre = strike(regular).size(48, 50).color(purple)
title = p.place(titre.text("  /ETC \nEclectic \nTech \nCarnival"))
title.frame(7,7,400,200)

fontasse = font.open('Inter-Regular.ttf')
times = strike(fontasse).size(28, 30).color(purple)
time = p.place(times.text("  8 - 13 \nΟκτωβρίου \n   2019"))
time.frame(120,16,400,200)

times_bis = strike(fontasse).size(28, 30).color(green)
time_bis = p.place(times_bis.text("  8 - 13 \nΟκτωβρίου \n   2019"))
time_bis.frame(121,17,400,200)

dates = strike(fontasse).size(22, 28).color(green)
date = p.place(dates.text(" Οχτώ, AMOQA\n& Communitism \n— Αθήνα, Ελλάδα"))
date.frame(18,98,80,200)

dates_bis = strike(fontasse).size(22, 28).color(purple)
date_bis = p.place(dates_bis.text(" Οχτώ, AMOQA\n& Communitism \n— Αθήνα, Ελλάδα"))
date_bis.frame(19,99,80,200)


for i in range(5,18,randint(4,5)):
    s = shape().stroke(purple).width(0.05)
    p.place(s.circle(-i+66, i+106, i))

for i in range(2,15,randint(1,2)):
    s = shape().stroke(purple).width(0.05)
    p.place(s.circle((-i/2)+209, i+90, i*0.75))

for i in range(5,20,randint(2,4)):
    s = shape().stroke(purple).width(0.05)
    p.place(s.circle(-i+172, i+1, i*0.5))

for i in range(5,20,randint(1,3)):
    s = shape().stroke(purple).width(0.05)
    p.place(s.circle(-i+203, i+35, i))

for i in range(5,20,randint(1,3)):
    s = shape().stroke(purple).width(0.05)
    p.place(s.circle(-i+97, i+52, i*0.75))

for i in range(10,50,randint(7,15)):
    s = shape().stroke(green).width(0.05)
    p.place(s.circle((i+10)+randint(-5,5), (i+10), i))

requin = image.open("etc_logo_default.jpg")
shark = p2.place(requin)
shark.frame(-60,116,200,70)
shark.fitwidth(115)

bloc1 = strike(fontasse).size(10.5, 13).color(purple)
block1 = p2.place(bloc1.text("Είμαστε μια φεμινιστική συλλογικότητα με ιδιαίτερη ιστορία, που χρόνια τώρα καταβροχθίζει τις ρίζες του ελέγχου και της κυριαρχίας.  \nΤο Εκλεκτικό Καρναβάλι ξεκίνησε το 2001, εμπνευσμένο από τις χακινγκ συναντήσεις που άρχισαν να λαμβάνουν χώρα στα τέλη της δεκαετίας του '90. Μέσα απο μερικές μέρες συναντήσεων, οι συμμετέχοντες γνωρίζονται και μιλάνε για υπολογιστές, τα δίκτυα, τον προγραμματισμό και την πολιτική \nπίσω από όλα αυτά, καθώς και να μοιραστούν δημιουργική εργασία (συμπεριλαμβανομένης και της τέχνης!).\n\nΤο Εκλεκτικό Καρναβάλι είναι ξεχωριστό διότι έχει μια φεμινιστική προσέγγιση που εμπλουτίζει τον πολιτικό προβληματισμό των χάκινγκ συναντήσεων με τη φεμινιστική \n                  διαθεματικότητα. "))
block1.frame(8,7,70,300)

bloc2 = strike(fontasse).size(10.5, 13).color(purple)
block2 = p2.place(bloc1.text("Γυναίκες, τρανς, ιντερσεξ και non-binary \n άτομα συνυπάρχουν μαζί με κάθε άλλο \n πλάσμα που συμφωνεί ενεργά με τον \n  φεμινισμό και την κριτική της εξουσίας \n   και της καταπίεσης.\n\n   Εκτός απο τα εργαστήρια, θα υπάρχει \n    χώρος για πολιτική και καλλιτεχνική \n     δραστηριοποίηση. Ο σκοπός μας είναι \n      να δημιουργήσουμε μελλοντικά σενάρια \n     βασισμένα στην αλληλεγγύη και την \n    κοινότητα.\n\n     Όλοι είναι εθελοντές και θα αναλάβουν \n      καθήκοντα κατα τη διάρκεια του \n       καρναβαλιού, όπως για παράδειγμα \n        βοηθώντας στη κουζίνα ή το γραφείο \n       πληροφοριών, καθαρίζοντας τους \n        κοινόχρηστους χώρους, μεταφράζοντας, \n         και βοηθώντας στην γενική διαχείριση."))
block2.frame(81,7,85,200)



dates_back = strike(regular).size(16, 18).color(green)
date_back = p2.place(dates_back.text("/ETC \n2019"))
date_back.frame(7,131,65,200)

"""
places_back = strike(regular).size(16, 18).color(green)
place_back = p2.place(places_back.text("October \n    8-13 \n       2019"))
place_back.frame(40,102,65,200)
"""

urls = strike(regular).size(13, 16).color(green)
lesurl = p2.place(urls.text(". https://eclectictechcarnival.org \n. https://8athens.wordpress.com \n. https://medium.com/@communitism \n. https://amoqa.net/"))
lesurl.frame(65,121,105,200)

logohaf = image.open("logo_haf.jpg")
block_logo = p2.place(logohaf)
block_logo.frame(174,119,200,70)
block_logo.fitwidth(30)



'''
for i in range(20):
    s = shape().stroke(black).width(0.1)
    p.place(s.circle(i, i, i))

for i in range(20):
    s = shape().stroke(black).width(0.1)
    p.place(s.circle(i+30, i+30, 20))


for i in range(20):
    s = shape().stroke(black).width(0.1)
    for i in range(8):
        p.place(s.ellipse(i+60, i+90, 15,10))
    for i in range(8, 16):
        p.place(s.ellipse(i+50, i+90, 13,10))
    for i in range(20,16,-1):
        p.place(s.ellipse(i+50, i+50, 11,10))

for i in range(7):
    s = shape().stroke(black).width(0.1)
    p.place(s.rectangle(randint(i,100), randint(i,100), randint(i,20), 30))
'''



'''
coordinates = [
    (10,70,10,70,50,80,90,140,90,10,)]

for cs in coordinates:
    p.place(t.polygon(cs))
'''


view(d.pdf("flyer_greek.pdf"))
