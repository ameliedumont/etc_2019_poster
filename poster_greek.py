from random import choice, random, randint
from PIL import Image
from flat import gray, rgb, cmyk, spot, diffuse, font, text, mesh, tree, \
    moveto, quadto, curveto, shape, strike, image, scene, group, document, view

"""
im = Image.open("base_etc.jpeg")
pixels = im.load()
iw, ih = im.size
for y in range(0,ih):
    for x in range(0,iw):
        random_color = randint(-2,2)
        r, g, b = pixels[x,y]
        pixels[x,y] = ( r*-random_color, g*random_color, b*random_color )

im.save("fond_detoure2.jpg")
"""

d = document(297, 420, 'mm')
p = d.addpage()
black = rgb(0, 0, 0)
purple = rgb(204, 0, 255)
blue = rgb(0, 153, 204)
green = rgb(0, 255, 153)

ciel = image.open("fond_detoure2.jpg")
block_ciel = p.place(ciel)
block_ciel.frame(37,67,200,70)
block_ciel.fitwidth(223)

halt = image.open("bluu01.jpg")
block_halt = p.place(halt)
block_halt.frame(210,155,200,70)
block_halt.fitwidth(50)

aand = image.open("bluu02.jpg")
block_and = p.place(aand)
block_and.frame(218,175,200,70)
block_and.fitwidth(40)

catch = image.open("bluu03.jpg")
block_catch = p.place(catch)
block_catch.frame(215,195,200,70)
block_catch.fitwidth(55)

fire = image.open("bluu04.jpg")
block_fire = p.place(fire)
block_fire.frame(229,215,200,70)
block_fire.fitwidth(37)

logo = image.open("logo_haf.jpg")
block_logo = p.place(logo)
block_logo.frame(40,241,200,70)
block_logo.fitwidth(35)

t = shape().stroke(blue).width(1)
p.place(t.line(0,230,148.5,160))
p.place(t.line(0,300,148.5,160))
p.place(t.line(0,420,148.5,160))
p.place(t.line(80,420,148.5,160))
p.place(t.line(148.5,420,148.5,160))
p.place(t.line(148.5,160,297,230))
p.place(t.line(148.5,160,297,300))
p.place(t.line(148.5,160,297,420))
p.place(t.line(148.5,160,217,420))
p.place(t.line(0,398,297,398))
p.place(t.line(0,328,297,328))
p.place(t.line(0,273,297,273))
p.place(t.line(0,233,297,233))
p.place(t.line(57,203,239,203))
p.place(t.line(95,183,201,183))

regular = font.open('SportingGrotesque-Regular.otf')
titre_bis = strike(regular).size(85, 85).color(green)
title_bis = p.place(titre_bis.text("      /ETC \n Eclectic Tech \n          Carnival"))
title_bis.frame(7,20,400,200)

titre = strike(regular).size(85, 85).color(purple)
title = p.place(titre.text("      /ETC \n Eclectic Tech \n          Carnival"))
title.frame(7,15,400,200)

fontasse = font.open('Inter-Regular.ttf')
times = strike(fontasse).size(48, 56).color(purple)
time = p.place(times.text("8 - 13 \nΟκτωβρίου \n  2019"))
time.frame(22,118,400,200)

times_bis = strike(fontasse).size(48, 56).color(green)
time_bis = p.place(times_bis.text("8 - 13 \nΟκτωβρίου \n  2019"))
time_bis.frame(24,117,400,200)

amoqa = strike(fontasse).size(38, 50).color(purple)
amoqab = p.place(amoqa.text("AMOQA"))
amoqab.frame(124,375,400,200)

amoqa2 = strike(fontasse).size(28, 50).color(purple)
amoqab2 = p.place(amoqa2.text("Πατησίων 14"))
amoqab2.frame(119,389,400,200)

places = strike(fontasse).size(28, 50).color(purple)
place = p.place(places.text("Πολυτεχνείου 8                                              Κεραμικού 28"))
place.frame(13.5,397,400,200)

dates = strike(fontasse).size(38, 50).color(purple)
date = p.place(dates.text("Οχτώ                                       Communitism"))
date.frame(13.5,381,400,200)



for i in range(0,150,randint(12,20)):
    s = shape().stroke(purple).width(0.05)
    p.place(s.circle(i+10, (i+10)*randint(1,3), i))


'''
for i in range(20):
    s = shape().stroke(black).width(0.1)
    p.place(s.circle(i, i, i))

for i in range(20):
    s = shape().stroke(black).width(0.1)
    p.place(s.circle(i+30, i+30, 20))


for i in range(20):
    s = shape().stroke(black).width(0.1)
    for i in range(8):
        p.place(s.ellipse(i+60, i+90, 15,10))
    for i in range(8, 16):
        p.place(s.ellipse(i+50, i+90, 13,10))
    for i in range(20,16,-1):
        p.place(s.ellipse(i+50, i+50, 11,10))

for i in range(7):
    s = shape().stroke(black).width(0.1)
    p.place(s.rectangle(randint(i,100), randint(i,100), randint(i,20), 30))
'''



'''
coordinates = [
    (10,70,10,70,50,80,90,140,90,10,)]

for cs in coordinates:
    p.place(t.polygon(cs))
'''


view(d.pdf("poster_greek.pdf"))
