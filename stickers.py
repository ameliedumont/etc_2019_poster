from random import choice, random, randint
from PIL import Image
from flat import gray, rgb, cmyk, spot, diffuse, font, text, mesh, tree, \
    moveto, quadto, curveto, shape, strike, image, scene, group, document, view

"""
im = Image.open("base_etc.jpeg")
pixels = im.load()
iw, ih = im.size
for y in range(0,ih):
    for x in range(0,iw):
        random_color = randint(-2,2)
        r, g, b = pixels[x,y]
        pixels[x,y] = ( r*-random_color, g*random_color, b*random_color )

im.save("fond_detoure2.jpg")
"""

d = document(297, 350, 'mm')
p = d.addpage()
black = rgb(0, 0, 0)
purple = rgb(204, 0, 255)
blue = rgb(0, 153, 204)
green = rgb(0, 255, 153)

"""
halt = image.open("bluu01.jpg")
block_halt = p.place(halt)
block_halt.frame(205,127,200,70)
block_halt.fitwidth(55)

aand = image.open("bluu02.jpg")
block_and = p.place(aand)
block_and.frame(210,149.5,200,70)
block_and.fitwidth(45)

catch = image.open("bluu03.jpg")
block_catch = p.place(catch)
block_catch.frame(215,172,200,70)
block_catch.fitwidth(60)

fire = image.open("bluu04.jpg")
block_fire = p.place(fire)
block_fire.frame(229,193.5,200,70)
block_fire.fitwidth(42)
"""

t = shape().stroke(blue).width(1)
p.place(t.line(0,230,148.5,160))
p.place(t.line(0,300,148.5,160))
p.place(t.line(0,420,148.5,160))
p.place(t.line(80,420,148.5,160))
p.place(t.line(148.5,420,148.5,160))
p.place(t.line(148.5,160,297,230))
p.place(t.line(148.5,160,297,300))
p.place(t.line(148.5,160,297,420))
p.place(t.line(148.5,160,217,420))




for i in range(15,250,20):
    s = shape().stroke(purple).width(1)
    p.place(s.circle(148, 160, i))


'''
for i in range(20):
    s = shape().stroke(black).width(0.1)
    p.place(s.circle(i, i, i))

for i in range(20):
    s = shape().stroke(black).width(0.1)
    p.place(s.circle(i+30, i+30, 20))


for i in range(20):
    s = shape().stroke(black).width(0.1)
    for i in range(8):
        p.place(s.ellipse(i+60, i+90, 15,10))
    for i in range(8, 16):
        p.place(s.ellipse(i+50, i+90, 13,10))
    for i in range(20,16,-1):
        p.place(s.ellipse(i+50, i+50, 11,10))

for i in range(7):
    s = shape().stroke(black).width(0.1)
    p.place(s.rectangle(randint(i,100), randint(i,100), randint(i,20), 30))
'''



'''
coordinates = [
    (10,70,10,70,50,80,90,140,90,10,)]

for cs in coordinates:
    p.place(t.polygon(cs))
'''


view(d.pdf("infini04.pdf"))
