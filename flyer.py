from random import choice, random, randint
from flat import gray, rgb, cmyk, spot, diffuse, font, text, mesh, tree, \
    moveto, quadto, curveto, shape, strike, image, scene, group, document, view

d = document(210, 148.5, 'mm')
p = d.addpage()
p2 = d.addpage()
black = rgb(0, 0, 0)
purple = rgb(204, 0, 255)
blue = rgb(0, 153, 204)
green = rgb(0, 255, 153)


ciel_fond = image.open("ciel.jpg")
block_cielfond = p.place(ciel_fond)
block_cielfond.frame(-5,-5,200,70)
block_cielfond.fitwidth(198)

ciel = image.open("base_detouree.jpg")
block_ciel = p.place(ciel)
block_ciel.frame(90,40,200,70)
block_ciel.fitwidth(105)

for i in range(6,27):
    s = shape().stroke(purple).width(0.05)
    p2.place(s.circle(-i+200, i+9, i))

halt = image.open("bluu01.jpg")
block_halt = p2.place(halt)
block_halt.frame(155,10,200,70)
block_halt.fitwidth(30)

aand = image.open("bluu02.jpg")
block_and = p2.place(aand)
block_and.frame(159,22.5,200,70)
block_and.fitwidth(25)

catch = image.open("bluu03.jpg")
block_catch = p2.place(catch)
block_catch.frame(163,35,200,70)
block_catch.fitwidth(32)

fire = image.open("bluu04.jpg")
block_fire = p2.place(fire)
block_fire.frame(167,46.5,200,70)
block_fire.fitwidth(23)

t = shape().stroke(blue).width(1)
p.place(t.line(0,120,175,0))
p.place(t.line(25,148.5,190,0))
p.place(t.line(85,148.5,205,0))
p.place(t.line(145,148.5,210,17))
p.place(t.line(200,148.5,210,80))
p.place(t.line(148.5,160,297,230))
p.place(t.line(148.5,160,297,300))
p.place(t.line(148.5,160,297,420))
p.place(t.line(148.5,160,217,420))
p.place(t.line(0,120,210,120))
p.place(t.line(51,85,210,85))
p.place(t.line(95,55,210,55))
p.place(t.line(128,32,210,32))
p.place(t.line(153,15,210,15))
p.place(t.line(171,3,210,3))

regular = font.open('SportingGrotesque-Regular.otf')
titre_bis = strike(regular).size(48, 50).color(green)
title_bis = p.place(titre_bis.text("  /ETC \nEclectic \nTech \nCarnival"))
title_bis.frame(9,10,400,200)

titre = strike(regular).size(48, 50).color(purple)
title = p.place(titre.text("  /ETC \nEclectic \nTech \nCarnival"))
title.frame(7,7,400,200)

fontasse = font.open('SelectricRoman.otf')
times = strike(fontasse).size(28, 30).color(purple)
time = p.place(times.text("October\n   8-13 \n   2019"))
time.frame(121,17,400,200)

times_bis = strike(fontasse).size(28, 30).color(green)
time_bis = p.place(times_bis.text("October\n   8-13 \n   2019"))
time_bis.frame(122,18,400,200)

dates = strike(fontasse).size(24, 28).color(green)
date = p.place(dates.text(" Eight, AMOQA\n& Communitism \n— Athens, Greece"))
date.frame(14,98,80,200)

dates_bis = strike(fontasse).size(24, 28).color(purple)
date_bis = p.place(dates_bis.text(" Eight, AMOQA\n& Communitism \n— Athens, Greece"))
date_bis.frame(15,99,80,200)


for i in range(5,18,randint(4,5)):
    s = shape().stroke(purple).width(0.05)
    p.place(s.circle(-i+66, i+106, i))

for i in range(2,15,randint(1,2)):
    s = shape().stroke(purple).width(0.05)
    p.place(s.circle((-i/2)+209, i+90, i*0.75))

for i in range(5,20,randint(2,4)):
    s = shape().stroke(purple).width(0.05)
    p.place(s.circle(-i+172, i+1, i*0.5))

for i in range(5,20,randint(1,3)):
    s = shape().stroke(purple).width(0.05)
    p.place(s.circle(-i+203, i+35, i))

for i in range(5,20,randint(1,3)):
    s = shape().stroke(purple).width(0.05)
    p.place(s.circle(-i+97, i+52, i*0.75))

for i in range(10,50,randint(7,15)):
    s = shape().stroke(green).width(0.05)
    p.place(s.circle((i+10)+randint(-5,5), (i+10), i))

bloc1 = strike(fontasse).size(12, 14).color(purple)
block1 = p2.place(bloc1.text("We are a collective body of feminists with a particular history, who chew on the roots of control and\
    domination.\n \nThe Eclectic Tech Carnival began in 2001, inspired by the hack meetings that\
    started taking place at the end of the 90’s. Spread Over several days, people meet \nin real life to talk about computers, networks, programming, and the politics behind it all, and to share creative work (art needs to get a mention here!)."))
block1.frame(8,8,65,200)

bloc2 = strike(fontasse).size(12, 14).color(purple)
block2 = p2.place(bloc1.text("  Besides workshops, there \nwill be space for people \nto be politically,\n performatively and\n  artistically active. Let us \n    create future scenarios \n     where the happy ending \n     is a small and local \n    world, based on \n    solidarity and community. \n    Everyone is a volunteer and \n     will take on tasks during \n      the event, for example \n       helping in the kitchen, \n        staffing the information \n        desk, cleaning the ablution \n        blocks, translating for each \n         other, infrastructure\n          administration, and so on."))
block2.frame(76,17,85,200)

requin = image.open("etc_logo_default.jpg")
shark = p2.place(requin)
shark.frame(-55,106,200,70)
shark.fitwidth(115)

dates_back = strike(regular).size(16, 18).color(green)
date_back = p2.place(dates_back.text("/ETC \n2019 \n@Athens"))
date_back.frame(7,123,65,200)

places_back = strike(regular).size(16, 18).color(green)
place_back = p2.place(places_back.text("October \n    8-13 \n       2019"))
place_back.frame(40,102,65,200)

urls = strike(regular).size(13, 16).color(green)
lesurl = p2.place(urls.text(". https://eclectictechcarnival.org \n. https://8athens.wordpress.com \n. https://medium.com/@communitism"))
lesurl.frame(65,126,105,200)

urls2 = strike(regular).size(13, 16).color(green)
lesurl2 = p2.place(urls2.text(". https://\namoqa.net/"))
lesurl2.frame(172,131.5,105,200)

logohaf = image.open("logo_haf.jpg")
block_logo = p2.place(logohaf)
block_logo.frame(169.5,103,200,70)
block_logo.fitwidth(30)



'''
for i in range(20):
    s = shape().stroke(black).width(0.1)
    p.place(s.circle(i, i, i))

for i in range(20):
    s = shape().stroke(black).width(0.1)
    p.place(s.circle(i+30, i+30, 20))


for i in range(20):
    s = shape().stroke(black).width(0.1)
    for i in range(8):
        p.place(s.ellipse(i+60, i+90, 15,10))
    for i in range(8, 16):
        p.place(s.ellipse(i+50, i+90, 13,10))
    for i in range(20,16,-1):
        p.place(s.ellipse(i+50, i+50, 11,10))

for i in range(7):
    s = shape().stroke(black).width(0.1)
    p.place(s.rectangle(randint(i,100), randint(i,100), randint(i,20), 30))
'''



'''
coordinates = [
    (10,70,10,70,50,80,90,140,90,10,)]

for cs in coordinates:
    p.place(t.polygon(cs))
'''


view(d.pdf("flyer.pdf"))
